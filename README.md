# WP FlourishAPI Plugin
Wordpressで[FlourishAPI](https://design-ac.backlog.com/git/DEVTOOL/flourish-website-module/tree/master)を使用するためのプラグイン。  

## How to use
`config.sample.php`をコピーして`config.php`を作成する。  

```php
<?php

return [
	'freedial' => [
		'name' => 'FlourishAPI\\WIFIMOBARECO\\freedialSetting',
		'path' => 'FlourishAPI/WIFIMOBARECOFreedial.php'
	],
	'route' => [
		'name' => 'FlourishAPI\\WIFIMOBARECO\\initRouteSetting',
		'path' => 'FlourishAPI/WIFIMOBARECORoute.php'
	],
];
```  

### Configuration
フリーダイヤルの設定関数とルートの設定関数の名前空間とファイルのパスをそれぞれ設定してください。  

その後このファイル一式をWordpressのplugins/flourishフォルダの中にコピーし、有効化してください。  

### Application
Wordpressテンプレート内で呼び出す場合は、下記のようにします。  

```php
	<div>
		<p>お電話からのお申込み・お問い合わせ</p>
		<p><?php echo do_shortcode('[free_dial]');?></p>
		<p>受付時間10:00~20:00(年末年始除く)</p>
	</div>
```  

ウィジェットや記事内でショートコードを利用することもできます。  
`[free_dial]`と入力することで表示することができます。  

## Requirements
PHP 5.6+  

