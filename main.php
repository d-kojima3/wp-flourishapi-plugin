<?php
/*
 Plugin Name:FlourishPlugin（改良版）
 Description:FlourishAPIからフリーダイアルを取得するプラグインです
 Author:K.
 Version:1.1
 */
 /*
 元々固定回線で使っていたものを改良
*/

require __DIR__.'/FlourishPlugin.php';

$virtual_document_root = str_replace($_SERVER['SCRIPT_NAME'], '', $_SERVER['SCRIPT_FILENAME']);

foreach (($config=require __DIR__.'/config.php') as $list) {
	if (!function_exists($list['name'])) {
		// FlourishAPIの読み込み
		if (!strpos(get_include_path(), $virtual_document_root."/../")) {
			// FlourishAPIのinclude_pathを設定
			set_include_path(get_include_path() . PATH_SEPARATOR . $virtual_document_root."/../");
		}
		require $list['path'];
	}
}

// main process
(new FlourishPlugin())->setFreecallFunc($config['freedial']['name'])
	->setRouteFunc($config['route']['name'])
	->init();
