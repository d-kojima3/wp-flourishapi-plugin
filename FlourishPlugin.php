<?php

/**
 * FDを取得するクラス
 * 
 */
class FlourishPlugin {

	private $freedial_func;
	private $route_func;

	private $fd;

	public function getFreeDial(){
		return $this->fd;
	}

	/**
	 * ショートコードの定義
	 *
	 * @param array $atts ショートコードの属性
	 */
	public function FreeDialShortCode()
	{
		return $this->fd;
	}

	/**
	 * initialization
	 *
	 * @return void
	 */
	public function init() {
		$this->fd = call_user_func($this->freedial_func);
		add_action('init', array($this,'getFreeDial'));
		add_shortcode('free_dial', array(&$this, 'FreeDialShortCode'));

		call_user_func($this->route_func);
	}

	/**
	 * set high-order func getting free-call number
	 *
	 * @param string $func_name
	 * @return FlourishPlugin
	 */
	public function setFreecallFunc($func_name) {
		$this->freedial_func = $func_name;
		return $this;
	}

	/**
	 * set high-order func getting routes
	 *
	 * @param string $func_name
	 * @return FlourishPlugin
	 */
	public function setRouteFunc($func_name) {
		$this->route_func = $func_name;
		return $this;
	}
}
