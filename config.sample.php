<?php
/**
 * サンプルファイルです
 * 設定は各自変更してください
 * 
 */
return [
	'freedial' => [
		'name' => 'FlourishAPI\\WIFIMOBARECO\\freedialSetting',
		'path' => 'FlourishAPI/WIFIMOBARECOFreedial.php'
	],
	'route' => [
		'name' => 'FlourishAPI\\WIFIMOBARECO\\initRouteSetting',
		'path' => 'FlourishAPI/WIFIMOBARECORoute.php'
	],
];
